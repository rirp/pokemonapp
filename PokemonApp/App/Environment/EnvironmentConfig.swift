//
//  Environment.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation

enum EnvKey: String {
    case baseURL = "BaseURL"
    case imgBaseURL = "ImgBaseURL"
}

struct EnvironmentConfig {
    static func infoForKey(_ key: EnvKey) -> String? {
        return (Bundle.main.infoDictionary?[key.rawValue] as? String)?
                .replacingOccurrences(of: "\\", with: "")
     }
}
