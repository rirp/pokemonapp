//
//  Item.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct Item: Hashable, Decodable {
    let name: String
}
