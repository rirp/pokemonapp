//
//  PkemonDetail.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 10/04/21.
//

import Foundation

struct Pokemon: Decodable {
    var name: String
    var height: Int
    var weight: Int
    var baseExperience: Int
    var types: Types?
    var moves: Moves?
    var sprites: Sprite?
    
    init(name: String = "", height: Int = 0,
         weight: Int = 0, baseExperience: Int = 0,
         types: Types? = nil, moves: Moves? = nil,
         sprites: Sprite? = nil) {
        self.name = name
        self.height = height
        self.weight = weight
        self.baseExperience = baseExperience
        self.types = types
        self.sprites = sprites
    }
    
    enum CodingKeys: String, CodingKey {
        case name, height, weight
        case baseExperience = "base_experience"
        case types, moves, sprites
    }
    
}

#if DEBUG || TESTING
let mockPokemon = Pokemon(name: "butterfree",
                          height: 11,
                          weight: 320,
                          baseExperience: 178,
                          types: mockTypes,
                          sprites: mockSprite)
#endif
