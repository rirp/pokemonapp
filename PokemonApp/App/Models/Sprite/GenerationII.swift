//
//  GenerationII.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct GenerationSpriteII: Codable, Spriteable {
    let crystal: SpriteItem?
    let gold: SpriteItem?
    let silver: SpriteItem?
}

#if DEBUG || TESTING

let mockGenerationIISprite = GenerationSpriteII(crystal: mockSpriteItem,
                                          gold: mockSpriteItem,
                                          silver: mockSpriteItem)
#endif
