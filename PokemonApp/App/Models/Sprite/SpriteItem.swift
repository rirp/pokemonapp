//
//  SpriteItem.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct SpriteItem: Codable {
    let frontDefault: String?
    
    enum CodingKeys: String, CodingKey {
        case frontDefault = "front_default"
    }
}


#if DEBUG || TESTING

let mockSpriteDefault = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"

let mockSpriteItem = SpriteItem(frontDefault: mockSpriteDefault)

#endif
