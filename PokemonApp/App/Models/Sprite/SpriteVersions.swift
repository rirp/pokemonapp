//
//  SpriteVersion.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import Foundation

struct SpriteVersions: Codable {
    let generationI     : GenerationSpriteI?
    let generationII    : GenerationSpriteII?
    let generationIII   : GenerationSpriteIII?
    let generationIV    : GenerationSpriteIV?
    
    enum CodingKeys: String, CodingKey {
        case generationI    = "generation-i"
        case generationII   = "generation-ii"
        case generationIII  = "generation-iii"
        case generationIV   = "generation-iv"
    }
}

#if DEBUG || TESTING

let mockVersionSprite = SpriteVersions(generationI: mockGenerationISprite,
                                       generationII: mockGenerationIISprite,
                                       generationIII: mockGenerationIIISprite,
                                       generationIV: mockGenerationIVSprite)

#endif
