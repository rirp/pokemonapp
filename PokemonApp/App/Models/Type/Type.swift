//
//  PokemonType.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 10/04/21.
//

import Foundation

typealias Types = [Type]

struct Type: Hashable, Decodable {
    let type: Item
    
    init(name: String) {
        self.type = Item(name: name)
    }
    
    func value() -> String {
        return self.type.name
    }
}

#if DEBUG || TESTING
let mockTypes: Types = [Type(name: "water"),
                        Type(name: "fire"),
                        Type(name: "ice"),
                        Type(name: "bug"),
                        Type(name: "fairy"),
                        Type(name: "poison"),
                        Type(name: "dragon"),
                        Type(name: "grass"),
                        Type(name: "ground")]
#endif
