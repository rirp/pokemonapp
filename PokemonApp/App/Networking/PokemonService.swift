//
//  PokemonApi.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation
import Combine

protocol PokemonServiceProtocol {
    func fetchPokemons(by generation: PKGeneration) -> AnyPublisher<Generation, Error>
    
    func fetchPokemonDetail(by id: String) -> AnyPublisher<Pokemon, Error>
}

class PokemonService: NetworkService {
    var session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
}

extension PokemonService: PokemonServiceProtocol {
    func fetchPokemonDetail(by id: String) -> AnyPublisher<Pokemon, Error> {
        let request = PokemonApiURL.detail(id).request()
        
        return execute(request, decodingType: Pokemon.self)
    }
    
    func fetchPokemons(by generation: PKGeneration) -> AnyPublisher<Generation, Error> {
        let request = PokemonApiURL.generation(generation).request()
        
        return execute(request, decodingType: Generation.self)
    }
}
