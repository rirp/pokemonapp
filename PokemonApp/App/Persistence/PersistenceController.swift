//
//  PersistenceController.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 29/04/21.
//

import CoreData

typealias PersistenceCompletion = (Error?) -> ()

struct PersistenceController {
    static let shared = PersistenceController()
    
    let container: NSPersistentContainer
    
    init() {
        container = NSPersistentContainer(name: "PokemonApp")
        container.loadPersistentStores { (_, error) in
            if let error = error {
                fatalError("Error \(error.localizedDescription)")
            }
        }
    }
    
    func save(completion: @escaping PersistenceCompletion = {_ in}) {
        let context = container.viewContext
        
        self.container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        if context.hasChanges {
            do {
                try context.save()
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
    
    func delete(_ object: NSManagedObject, completion: @escaping PersistenceCompletion = {_ in}) {
        let context = container.viewContext
        context.delete(object)
        save(completion: completion)
    }
}
