//
//  PokemonEntity+Observable.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 30/04/21.
//

import Foundation

class PokemonEntityObservable: ObservableObject {
    @Published var votes: [PokemonEntity] = []
    
    init() {
        votes = []
    }
}
