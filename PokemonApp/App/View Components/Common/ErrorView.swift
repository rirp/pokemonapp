//
//  ErrorView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 19/04/21.
//

import SwiftUI

struct ErrorView: View{
    @Binding var message: String?
    
    var body: some View {
        VStack {
            LottieView(name: "lottie_error")
                .frame(width: 250, height: 250)
            Text(self.message ?? "Unknown Error")
                .font(.title)
                .foregroundColor(Color.red)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.yellow.opacity(0.3))
        .edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(message: .constant(""))
    }
}
#endif
