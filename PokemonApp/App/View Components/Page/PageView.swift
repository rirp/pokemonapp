//
//  PageView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import SwiftUI

struct PageView: View {
    
    let heightScreen = UIScreen.main.bounds.height
    let widthScreen = UIScreen.main.bounds.width
    
    @State var title: String
    @State var image: String
    
    private var color: Color? {
        Color.types.randomElement()
    }
    
    var body: some View {
        
        VStack {
            HStack {
                Spacer()
                
                ImageLoadableView(
                    url: URL(string: image)!,
                    placeholder: { ActivityIndicator() }
                )
                .scaledToFit()
                .frame(height: heightScreen / 3)
                .padding(.top)
                
                Spacer()
            }
            Text(title)
                .font(.title)
                .foregroundColor(.white)
                .padding()
        }
        .background(color)
        .cornerRadius(25)
        .shadow(radius: 10)
        .padding()
    }
}

#if DEBUG
struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(title: "Pikachu", image: mockSpriteDefault)
    }
}
#endif
