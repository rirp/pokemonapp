//
//  PageView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import SwiftUI

struct SwiperView: View {
    
    private var pages: [Specie]
    
    @Binding private var index: Int
    @State private var offset: CGFloat = 0
    @State private var isUserSwiping: Bool = false
    
    @EnvironmentObject var votes: PokemonEntityObservable
    @EnvironmentObject var isLikeShowing: LikeShowingObservable
    
    @ObservedObject var viewModel = SwiperViewModel()
    
    init(pages: [Specie], index: Binding<Int>) {
        self.pages = pages
        _index = index
    }
    
    var body: some View {
        content
    }
    
    @ViewBuilder
    func page(viewData: Specie, width: CGFloat, height: CGFloat) -> some View {
        PageView(
            title: viewData.name ?? "",
            image: viewData.url
        )
        .frame(width: width,
               height: height)
    }
    
    var content: some View {
        GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .center, spacing: 0) {
                    ForEach(self.pages, id: \.self) { viewData in
                        page(viewData: viewData,
                             width: geometry.size.width,
                             height: geometry.size.height)
                    }
                }
            }
            .content
            .offset(x: self.isUserSwiping ? self.offset : CGFloat(self.index) * -geometry.size.width)
            .frame(width: geometry.size.width, alignment: .leading)
            .gesture(
                DragGesture()
                    .onChanged({ value in
                        onChangeGesture(value, geometry: geometry)
                    })
                    .onEnded({ value in
                        onEnded(value, geometry: geometry)
                    })
            )
        }
        .onAppear {
            viewModel.setup(votes: votes)
        }
        .onTapGesture(count: 2) {
            viewModel.saveFavorite(pokemon: pages[index])
            isLikeShowing.isLikeShowing.toggle()
        }
    }
    
    
    func onChangeGesture(_ value: DragGesture.Value, geometry: GeometryProxy) {
        self.isUserSwiping = true
        self.offset = value.translation.width + -geometry.size.width * CGFloat(self.index)
    }
    
    func onEnded(_ value: _ChangedGesture<DragGesture>.Value, geometry: GeometryProxy) {
        if value.predictedEndTranslation.width < geometry.size.width / 2, self.index < self.pages.count - 1 {
            self.index += 1
        }
        if value.predictedEndTranslation.width > geometry.size.width / 2, self.index > 0 {
            self.index -= 1
        }
        withAnimation {
            self.isUserSwiping = false
        }
    }
    
}

#if DEBUG
struct SwiperView_Previews: PreviewProvider {
    static var previews: some View {
        SwiperView(pages: [], index: .constant(0))
    }
}
#endif
