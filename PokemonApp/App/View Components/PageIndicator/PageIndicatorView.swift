//
//  PageIndicatorView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import SwiftUI

// MARK: - Page Indicator -
struct PageIndicatorView: View {
    // Constants
    private let spacing: CGFloat = 2
    private let diameter: CGFloat = 8
    
    // Settings
    let numPages: Int
    
    @EnvironmentObject var selectedIndex: CurrentIndex
    
    init(numPages: Int) {
        self.numPages = numPages
    }
    
    var body: some View {
        VStack {
            HStack(alignment: .center, spacing: spacing) {
                ForEach((0..<numPages)) {
                    DotIndicatorView(
                        pageIndex: $0,
                        selectedPage: self.$selectedIndex.index
                    ).frame(
                        width: self.diameter,
                        height: self.diameter
                    )
                }
            }
        }
    }
}

#if DEBUG
struct PageIndicator_Previews: PreviewProvider {
    static var previews: some View {
        PageIndicatorView(numPages: 5)
            .previewDisplayName("Regular")
            .previewLayout(PreviewLayout.sizeThatFits)
            .padding()
            .environmentObject(CurrentIndex())
    }
}
#endif
