//
//  PikachuView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 17/04/21.
//

import SwiftUI

import SwiftUI

struct PikachuView: View {
    
    let tailSize: CGFloat = 200.0
    let pikachuSize: CGFloat = 250.0
    
    @State var tail = false
    @State var shockOne = false
    @State var shockTwo = false
    @State var visibilityOpacity = false
    @State var titleShadow = false
    
    var shockAnimation: Animation {
        Animation.interpolatingSpring(stiffness: 30,damping: 10)
            .speed(3)
            .repeatForever(autoreverses: true)
            .delay(1)
    }
    
    var tailAnimation: Animation {
        Animation
            .easeInOut(duration: 1)
            .delay(3)
            .repeatForever(autoreverses: true)
    }
    
    var eyesAnimation: Animation {
        Animation
            .easeInOut(duration: 0.2)
            .delay(2)
            .repeatForever(autoreverses: true)
    }
    
    var tailView: some View {
        Image("image3")
            .resizable()
            .frame(width: tailSize, height: tailSize, alignment: .center)
            .shadow(color: .yellow, radius: 0.5, x: 0.0, y: 0.0)
            .rotationEffect(.degrees(9))
            .offset(x: 80, y: 0)
            .rotationEffect(.degrees(tail ? 0 : 45))
            .animation(tailAnimation)
            .onAppear(){
                self.tail.toggle()
            }
    }
    
    var bodyCloseEyes: some View {
        Image("image1")
            .resizable()
            .frame(width: pikachuSize, height: pikachuSize, alignment: .center)
            .shadow(color: .yellow, radius: 0.5, x: 0.0, y: 0.0)
    }
    
    var bodyOpenEyes: some View {
        Image("image2")
            .resizable()
            .frame(width: pikachuSize, height: pikachuSize, alignment: .center)
            .shadow(color: .yellow, radius: 0.5, x: 0.0, y: 0.0)
            .opacity(visibilityOpacity ? 1 : 0)
            .animation(eyesAnimation)
            .onAppear() {
                self.visibilityOpacity.toggle()
            }
    }
    
    var shock1View: some View {
        Image("shock1")
            .resizable()
            .frame(width: pikachuSize, height: pikachuSize)
            .shadow(color: .yellow, radius: 2, x: 0.0, y: 0.0)
            .opacity(shockOne ? 1 : 0)
            .clipShape(Circle().offset(x: shockOne ? 0 : 125))
            .animation(shockAnimation)
            .onAppear() {
                self.shockOne.toggle()
            }
    }
    
    var shock2View: some View {
        Image("shock2")
            .resizable()
            .frame(width: pikachuSize, height: pikachuSize)
            .shadow(color: .yellow, radius: 2, x: 0.0, y: 0.0)
            .opacity(shockTwo ? 1 : 0)
            .clipShape(Circle().offset(x: shockTwo ? 0 : -125))
            .animation(shockAnimation)
            .onAppear() {
                self.shockTwo.toggle()
            }
    }
    
    var body: some View {
        VStack {
            ZStack{
                //MARK:- tail
                tailView
                
                //MARK:- body : close eyes
                bodyCloseEyes
                
                //MARK:- body : open eyes
                bodyOpenEyes
                
                //MARK:- shocks
                shock1View
                shock2View
            }
            Text("Loading . . .")
                .font(.title3)
                .fontWeight(.thin)
        }
    }
}

#if DEBUG
struct PikachuView_Previews: PreviewProvider {
    static var previews: some View {
        PikachuView()
    }
}
#endif
