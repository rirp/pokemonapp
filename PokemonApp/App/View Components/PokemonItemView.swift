//
//  PokemonItemView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 22/04/21.
//

import SwiftUI

struct PokemonItemView: View {
    var pokemon: Specie
    
    var body: some View {
        NavigationLink(destination: DetailView(name: pokemon.name ?? "")) {
            VStack {
                ImageLoadableView(
                    url: URL(string: pokemon.url)!,
                    placeholder: { ActivityIndicator() },
                    image: { Image(uiImage: $0).resizable() }
                )
                Text(pokemon.name?.firstCapitalized ?? "")
                    .font(.footnote)
                    .foregroundColor(.white)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding()
            .background(Color.green.opacity(0.50))
            .cornerRadius(10)
            .shadow(radius: 5)
        }
    }
}

#if DEBUG
struct PokemonItemView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonItemView(pokemon: Specie(name: "", _url: ""))
    }
}
#endif
