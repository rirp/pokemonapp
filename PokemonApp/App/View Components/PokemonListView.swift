//
//  PokemonListView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 14/04/21.
//

import SwiftUI

struct PokemonListView: View {
    @EnvironmentObject var collection: Collection
    
    var generation: PKGeneration
    
    var columns: [GridItem] =
        Array(repeating: .init(.flexible()), count: 3)
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(collection.collection[generation] ?? [],
                        id: \.self,
                        content: PokemonItemView.init)
            }
            .padding()
        }
    }
}

#if DEBUG
struct PokemonListView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonListView(generation: .i)
            .environmentObject(Collection())
        
    }
}
#endif
