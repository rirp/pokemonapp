//
//  MovesView.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 25/04/21.
//

import SwiftUI

struct SpritesView: View {
    @EnvironmentObject var collection: Collection
    
    @ObservedObject var sprites: SpriteObservable
    
    @State private var currentIndex: CurrentIndex = CurrentIndex()
    
    var body: some View {
        content
    }
    
    var content: some View {
        imageContainerView(sprites.sprite?.by(collection.generationSelected).count,
                           sprites: sprites.sprite?.by(collection.generationSelected),
                           color: sprites.color)
    }
    
    @ViewBuilder
    func imageContainerView(
        _ numberOfImages: Int?,
        sprites: [String]?, color: String?
    ) -> some View {
        
        GeometryReader { geometry in
            VStack {
                ImageCarouselView(numberOfImages: numberOfImages ?? 0) {
                    ForEach(sprites ?? [], id: \.self) { value in
                        
                        ImageLoadableView(
                            url: URL(string: value)!,
                            placeholder: { ActivityIndicator() },
                            image: { Image(uiImage: $0).resizable() }
                        )
                        .clipShape(
                            Circle()
                        )
                        .shadow(radius: 10)
                        .overlay(Circle().stroke(Color.white, lineWidth: 5))
                        .frame(width: geometry.size.width,
                               height: geometry.size.height * 0.8)
                    }
                }
                
                PageIndicatorView(numPages: numberOfImages ?? 0)
                    .padding(.bottom)
            }
            
        }
        .environmentObject(currentIndex)
        .edgesIgnoringSafeArea(.top)
        .background(color != nil ? Color(color!) : Color.white)
    }
}
#if DEBUG
struct MovesView_Previews: PreviewProvider {
    static var previews: some View {
        SpritesView(sprites: SpriteObservable(sprite: mockSprite, color: "bug"))
            .environmentObject(Collection())
    }
}
#endif
