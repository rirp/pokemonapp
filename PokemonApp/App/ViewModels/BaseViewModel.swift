//
//  BaseViewModel.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 21/04/21.
//

import Foundation
import CoreData

protocol BaseViewModelProtocol: ObservableObject {
    // MARK: Loading state
    var isLoadingShowing: Bool { get set }
    
    // MARK: Error values
    
    var isShowingError: Bool { get set }
    
    var error: String? { get set }
    
    func removeLoading()
    
    func showError(message: String?)
}

class BaseViewModel {
    
    @Published var isLoadingShowing = false
    
    @Published var isShowingError = false

    @Published var error: String?

}

extension BaseViewModel: BaseViewModelProtocol {
    func removeLoading() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.isLoadingShowing = false
        })
    }
    
    func showError(message: String?) {
        self.isShowingError = message != nil
        self.error = message
    }
}
