//
//  DetailViewModel.swift
//  PokemonApp
//
//  Created by Ronald Ivan Ruiz Poveda on 21/04/21.
//

import Foundation
import Combine

class DetailViewModel: BaseViewModel {
    private var cancellable: AnyCancellable?
    
    private var name: String
    
    private var pokemonService: PokemonServiceProtocol!
    
    @Published var detail: Pokemon?
    
    
    init(name: String, pokemonService: PokemonServiceProtocol = PokemonService()) {
        self.name = name
        self.pokemonService = pokemonService
    }
    
    func fetchPokemonDetail() {
        self.isLoadingShowing = true
        cancellable = pokemonService.fetchPokemonDetail(by: name)
            .sink(receiveCompletion: { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                
                self?.removeLoading()
                self?.showError(message: error.localizedDescription)
            }, receiveValue: { [weak self] response in
                self?.detail = response
                self?.removeLoading()
            })
    }
}
