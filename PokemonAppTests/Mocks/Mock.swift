//
//  Mock.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 11/04/21.
//

import Foundation
@testable import PokemonApp

class Mock {
    static let generationUrlValue = "https://pokeapi.co/api/v2/pokemon-species/1/"
    static let localhostURL = URL(string: "http://localhost:8080")!
    
    static let internalErrorResponse = HTTPURLResponse(url: localhostURL,
                                                       statusCode: 500,
                                                       httpVersion: nil,
                                                       headerFields: nil)
    
    static let successResponse = HTTPURLResponse(url: localhostURL,
                                                 statusCode: 200,
                                                 httpVersion: nil,
                                                 headerFields: nil)
    
    static let pokemonDetailJson = FileHelper.readJsonAsString(Pokemon.self)
    
    static let pokemonDetailData = FileHelper.readJsonAsData(Pokemon.self)
    
    static let generationData = FileHelper.readJsonAsData(Generation.self)
    
    static let pokemonData = FileHelper.readJsonAsData(Specie.self)
    
    static let generationI = Generation(id: 1, name: "generation-i",
                                        pokemons: [Specie(name: "bulbasur",
                                                          _url: generationUrlValue),
                                                   Specie(name: "ivisour",
                                                          _url: generationUrlValue),
                                                   Specie(name: "pikachu",
                                                          _url: generationUrlValue)])
    
    static func session() -> URLSession {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        return URLSession(configuration: config)
    }
}
