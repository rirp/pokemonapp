//
//  PokemonTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 14/04/21.
//

import XCTest
@testable import PokemonApp

class PokemonTest: XCTestCase {
    let urlExcepted = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPokemon_WhenURLHasAValidValue_ReturnAId() {
        let sut = Specie(name: "bulbasor", _url: "https://pokeapi.co/api/v2/pokemon-species/1/")
        
        XCTAssertEqual(sut.url, urlExcepted)
    }

    func testPokemon_WhenURLIsInvalid_ShouldReturnNil() {
        let sut = Specie(name: "bulbasor", _url: "")
        XCTAssertEqual(sut.url, "", "The value returns a something and it should return nil")
    }

    func testPokemon_WhenURLValueIsAURL() {
        let sut = Specie(name: "bulbasor", _url: "https://pokeapi.co/api/v2/pokemon-species/")
        let url = URL(string: sut.url)
        XCTAssertNotNil(url)
    }

}
