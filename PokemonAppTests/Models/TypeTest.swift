//
//  TypeTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 28/04/21.
//

import XCTest
@testable import PokemonApp

class TypeTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testType_WhenTypeIsProvided_ShouldNotBeNil() throws {
        let sut = Type(name: "grass")
        XCTAssertEqual(sut.value(), "grass")
    }

}
