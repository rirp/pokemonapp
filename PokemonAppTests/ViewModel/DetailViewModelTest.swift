//
//  DetailViewModelTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 21/04/21.
//

import XCTest
@testable import PokemonApp

class DetailViewModelTest: XCTestCase {

    var sut: DetailViewModel!
    var pokemonService: PokemonServiceProtocol!
    
    override func setUpWithError() throws {
        pokemonService = PokemonServiceMock()
        sut = DetailViewModel(name: "bulbasaur", pokemonService: pokemonService)
        PokemonServiceMock.pokemonDetail = mockPokemon
    }

    override func tearDownWithError() throws {
        PokemonServiceMock.pokemonDetail = nil
        PokemonServiceMock.error = nil
        pokemonService = nil
        sut = nil
    }

    func testDetailViewModel_WhenPokemonServiceApiResponseSuccessful_ReturnPokemonDetail() {
        sut.fetchPokemonDetail()
        XCTAssertNotNil(sut.detail)
        XCTAssertNotNil(sut.detail?.types)
    }
    
    func testDetailViewModel_WhenPokemonServiceApiFailure_ReturnError() {
        PokemonServiceMock.error = APIError.unexceptedError
        
        sut.fetchPokemonDetail()
        
        XCTAssertNil(sut.detail)
        XCTAssertNotNil(sut.error)
    }

}
