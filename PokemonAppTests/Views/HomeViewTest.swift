//
//  HomeViewTest.swift
//  PokemonAppTests
//
//  Created by Ronald Ivan Ruiz Poveda on 27/04/21.
//

import XCTest
//import ViewInspector
import SwiftUI
@testable import PokemonApp


class HomeViewTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHomeView_WhenGetAError_ShouldShowErrorView() throws {
        let mock = HomeViewModelMock()
        mock.error = "A error "
        mock.isShowingError = true
        mock.isLoadingShowing = false
        
       var sut = HomeView(viewModel: mock)
//        let exp = sut.on(\.didAppear) { view in
//            XCTAssertFalse(try view.actualView().viewModel.isLoadingShowing)
//            ViewHosting.expel()
//        }
//        ViewHosting.host(view: sut)
//        wait(for: [exp], timeout: 0.1)
//        
        let hostingVC = UIHostingController(rootView: sut)
        
    }

}
